package clids.ex2.Order;



import java.io.*;

public class OrderName implements Order {

    public OrderName() {
    }

    @Override
    public void orderFiles(File[] files) {
        int min;
        File temp;
        for (int i = 0; i < files.length; i++) {
            min = findMin(i, files);
            temp = files[i];
            files[i] = files[min];
            files[min] = temp;
        }
    }

    private int findMin(int range, File[] files) {


        int min = range;
        for (int i = range; i < files.length; i++) {
            if (files[i].getName().compareTo(files[min].getName()) < 0) {
                min = i;
            }
            if (files[i].getName().compareTo(files[min].getName()) == 0) {
                if (ifEquels(i, min, files) < 0) {
                    min = i;
                }
            }

        }
        return min;
    }

    private int ifEquels(int i, int k, File[] files) {
        return files[i].getAbsolutePath().compareTo(files[k].getAbsolutePath());
    }
}

