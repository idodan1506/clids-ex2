package clids.ex2.Order;

import java.io.File;


public class OrderSize implements Order
{
    public OrderSize()
	{
        }

    @Override
	public void orderFiles(File[] files)
	{
		int min;
		File temp;
		for(int i=0;i<files.length;i++){
			 min=findMin(i, files);
			temp=files[i];
			files[i]=files[min];
			files[min]=temp;
		}
	}
	private int findMin(int range,File[] files){
		
		
		int min=range;
		for(int i=range;i<files.length;i++){
			if(files[i].length()-files[min].length() <0){
				min=i;
			}
			if(files[i].length()-files[min].length()==0){
				if(ifEquels(i, min,files)<0){
					min=i;
				}
			}
			
		}
		return min;
	}
	private int ifEquels (int i, int k,File[] files){
		return files[i].getAbsolutePath().compareTo(files[k].getAbsolutePath());
	}

}
