package clids.ex2.Order;

import java.io.*;
public class OrderPath implements Order
{
	public OrderPath()
	{
        }

        @Override
	public void orderFiles(File[] files)
	{
		int min;
		File temp;
		for(int i=0;i<files.length;i++){
			 min=findMin(i,files);
			temp=files[i];
			files[i]=files[min];
			files[min]=temp;
		}
	}
	private int findMin(int range,File[] files){
		
		
		int min=range;
		for(int i=range;i<files.length;i++){
			if(files[i].getAbsolutePath().compareTo(files[min].getAbsolutePath())<0){
				min=i;
			}
			
			
		}
		return min;
	}

}
