/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2;

import clids.ex2.Actions.Action;
import clids.ex2.Exceptions.BadSubsectionException;
import clids.ex2.Exceptions.FailingException;
import clids.ex2.Exceptions.FirstLineException;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Manager {
/**
 * the topmost part of the code. coordinates all the other parts,
 * the parser and section.  in addition this is where we get the files 
 * from the "args" and into in-program variables.
 * @param args 
 */
    public static void manage(String[] args) {
        if (args.length != 2) {
            System.err.println("ERROR");
            System.exit(-1);
        }

        File sourceDir = new File(args[0]);
        File commandFile = new File(args[1]);
        Parser parser = new Parser(commandFile,sourceDir);//invoking parser
        ArrayList<Section> sectionHolder = new ArrayList<>();

        try {
            sectionHolder = parser.parse();
        } catch (BadSubsectionException | FirstLineException e) {
            e.handle();
        }

        for (Section sec : sectionHolder) {//sections
            for (String warning : sec.getErrors()) {
                System.err.println(warning);
            }
            for (String comment : sec.getComments()) {
                System.out.println(comment);
            }

            // getting filtered files
            ArrayList<File> filteredFiles = FilterFactory.getFilteredFiles(sourceDir, sec.getFilter());
            File[] filteredFileArray = ToolBox.returnFileArray(filteredFiles);
            sec.getOrder().orderFiles(filteredFileArray);

            for (Action action: sec.getActions()) {
                for (File file : filteredFileArray) {
                    try {
                        action.doAction(file);
                    } catch (FailingException e) {
                        e.handle(action.getLine());
                    }
                }
            }
        }



    }
}
