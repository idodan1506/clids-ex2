/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2;

import clids.ex2.Actions.Action;
import clids.ex2.Filters.Filter;
import clids.ex2.Order.Order;
import java.util.ArrayList;

/**
 * A data Type that contains all the necessary information from one section
 *
 * @author Administrator
 */
public class Section {

    // holding the params
    private ArrayList<String> comments;
    private ArrayList<Action> actions;
    private Filter filter;
    private Order order;
    private ArrayList<String> errors;

    /**
     * Default Ctor, creates empty actions, comments and actionLines arrayLists,
     * empty Filter for filter and empty Order for order and orderLine and
     * filterLine with an initial value of -1.
     */
    public Section() {
        this.actions = new ArrayList<>();
        this.comments = new ArrayList<>();
        this.filter = ToolBox.DEFAULT_FILTER;
        this.order = ToolBox.DEFAULT_ORDER;
        errors = new ArrayList<>();
    }

    /**
     * Creates a Section from given inputs
     *
     * @param actions - an arrayList of the actions
     * @param comments - an arrayList of the comments
     * @param filter - the filter of the section
     * @param order - the order of the section
     * @param actionsLines - the action lines
     */
    public Section(ArrayList<Action> actions, ArrayList<String> comments,
            Filter filter, Order order, ArrayList<Integer> actionsLines) {
        this.actions = actions;
        this.comments = comments;
        this.filter = filter;
        this.order = order;
    }

    /**
     * Add an Action
     *
     * @param action - the action to add
     */
    public void addAction(Action action) {
        actions.add(action);
    }

    /**
     * Add a comment String
     *
     * @param comment - the comment to add
     */
    public void addComment(String comment) {
        comments.add(comment);
    }

    /**
     * Set the filter
     *
     * @param filter - the filter
     */
    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * Get the filter of section
     *
     * @return the filter String
     */
    public Filter getFilter() {
        return this.filter;
    }

    /**
     * Set the order
     *
     * @param order - the order
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * Returns the order of the section
     *
     * @return - the string order of the section
     */
    public Order getOrder() {
        return this.order;
    }

    /**
     * Returns the arrayList of the comments
     *
     * @return - the arrayList of the comments
     */
    public ArrayList<String> getComments() {
        return (ArrayList<String>) comments.clone();
    }

    /**
     * Returns the arrayList of the actions
     *
     * @return - the arrayList of the actions
     */
    public ArrayList<Action> getActions() {
        return (ArrayList<Action>) actions.clone();
    }

    /**
     * Adds a new error
     *
     * @param err - the error
     */
    public void addError(String err) {
        errors.add(err);
    }

    /**
     * Returns the arrayList of the errors
     *
     * @return - the arrayList of the errors
     */
    public ArrayList<String> getErrors() {
        return (ArrayList<String>) errors.clone();
    }
}
