/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2;

/**
 *
 * @author t7638506
 */
import clids.ex2.Exceptions.BadFilterNameException;
import clids.ex2.Exceptions.BadInputParamsException;
import clids.ex2.Filters.*;
import java.io.*;
import java.util.*;

public class FilterFactory {

    /**
     *
     * @param toDo
     * @return returns a filter that accepts all the files who answers some
     * given conditions.
     * @throws BadFilterNameException when given a bad name for the filter
     * @throws BadInputParamsException when given a bad param.
     */
    public static Filter getFilter(String toDo) throws BadFilterNameException, BadInputParamsException {
        if (toDo.endsWith("NOT")) {
            Filter filter = getFilter(toDo.substring(0, toDo.length() - 4));
            Filter Notfilter = new NotFilter(filter);
            return Notfilter;
        }
        int reader;
        reader = toDo.indexOf('%');
        String command;
        command = toDo.substring(0, reader);
        String values = toDo.substring(reader + 1);
        return getFilter(command, values);
    }

    private static Filter getFilter(String command, String values) throws BadFilterNameException, BadInputParamsException {
        Filter filter = null;
        int reader;
        switch (command) {// figuring wich filter it need to be.
            case "after":
                Date afterBar = ToolBox.createDateFromStringByFormat(values);
                filter = new AfterFilter(afterBar);
                break;
            case "before":
                Date beforeBar = ToolBox.createDateFromStringByFormat(values);
                filter = new BeforeFilter(beforeBar);
                break;
            case "between":
                reader = values.indexOf('%');
                double lower = Double.parseDouble(values.substring(0, reader));
                double upper = Double.parseDouble(values.substring(reader + 1));
                if (lower > upper) {
                    throw new BadInputParamsException();
                }
                filter = new BetweenFilter(lower, upper);
                break;

            case "executable":
                boolean canExec = false;
        switch (values) {
            case "YES":
                canExec = true;
                filter = new ExecutableFilter(canExec);
                break;
            case "NO":
                canExec = false;
                break;
            default:
                throw new BadInputParamsException();
        }
                filter = new ExecutableFilter(canExec);
                break;

            case "file":
                String name = values;
                filter = new FileNameFilter(name);
                break;
            case "prefix":
                String prefix = values;
                filter = new PrefixFilter(prefix);
                break;
            case "suffix":
                String suffix = values;
                filter = new SuffixFilter(suffix);
                break;
            case "greater_than":
                double greater_then_this = Double.parseDouble(values);
                filter = new GreaterThanFilter(greater_then_this);
                break;
            case "writable":
                boolean canWrite = false;
        switch (values) {
            case "YES":
                canWrite = true;
                filter = new WritableFilter(canWrite);
                break;
            case "NO":
                canWrite = false;
                break;
            default:
                throw new BadInputParamsException();
        }
                filter = new WritableFilter(canWrite);
                break;
            //catch bad value
            case "smaller_than":
                double smaller_then_this = Double.parseDouble(values);
                filter = new SmallerThanFilter(smaller_then_this);
                break;
            case "hidden":
                boolean isHidden = false;
        switch (values) {
            case "YES":
                isHidden = true;
                filter = new HiddenFilter(isHidden);
                break;
            case "NO":
                isHidden = false;
                break;
            default:
                throw new BadInputParamsException();
        }
                filter = new HiddenFilter(isHidden);
                break;
                default:
                    throw new BadFilterNameException();
        }
        
        return filter;
    }

    /**
     * a method to return all the files in a given directory by recursive calls
     *
     * @param file
     * @return an array list of the files.
     */
    public static ArrayList<File> getFilteredFiles(File file, Filter filter) {
        ArrayList<File> OnlyFiles = new ArrayList();
        recrusiveFile(file, OnlyFiles, filter);
        return OnlyFiles;
    }

    private static void recrusiveFile(File file, ArrayList<File> OnlyFiles, Filter filter) {
        //getting all files that sits in a parent directory.
        if (file.isFile()) {
            OnlyFiles.add(file);
        } else {
            File[] kids = file.listFiles(filter);
            for (int i = 0; i < kids.length; i++) {
                recrusiveFile(kids[i], OnlyFiles, filter);
            }
        }

    }
}
