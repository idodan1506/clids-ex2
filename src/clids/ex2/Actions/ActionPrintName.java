/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Actions;

import java.io.File;

/**
 *
 * @author t7638506
 */
public class ActionPrintName implements Action {

    int line;
/**
     * a standart Ctor
     *
     * @param line the line of the command file where this action was written
     */
    public ActionPrintName(int line) {
        this.line = line;
    }

    @Override
    public void doAction(File file) {
        System.out.println(file.getName());

    }


    /**
     * returns the line in the command file
     * @return the line
     */
    @Override
    public int getLine() {
        return line;
    }
}
