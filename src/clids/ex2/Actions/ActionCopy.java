 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Actions;

import clids.ex2.Exceptions.*;
import clids.ex2.ToolBox;
import java.io.*;

/**
 *
 * @author t7638506
 */
public class ActionCopy implements Action {

    File targetDir;
    int line;

    /**
     * a standardt Ctor getting the destionation and the line of the command
     * file
     *
     * @param targetDir-the destionation
     * @param line hte line in the command file.
     */
    public ActionCopy(File targetDir, int line) {
        this.line = line;
        this.targetDir = targetDir;
    }

    /**
     *
     * @param fileToCopy
     * @throws BadInputParamsException
     */
    @Override
    public void doAction(File fileToCopy) throws FailingException {
        this.targetDir.mkdirs();
        ToolBox.copy(fileToCopy, new File(targetDir, fileToCopy.getName()));
    }

    /**
     * returns the line in the command file
     * @return the line
     */
    @Override
    public int getLine() {
        return line;
    }
}
