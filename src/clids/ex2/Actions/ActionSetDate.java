package clids.ex2.Actions;

import clids.ex2.Exceptions.FailingException;
import java.io.*;

public class ActionSetDate extends ActionSet {

    int line;
    long setTo;
/**
 * a stnadardt Ctor
 * @param setTo the date to set to
 * @param line the line of the command file
 */
    public ActionSetDate(long setTo, int line) {
        this.line = line;
        this.setTo = setTo;
    }

/**
 * the action to do
 * @param file to set its state
 * @throws FailingException 
 */
    @Override
    public void doAction(File file) throws FailingException {
        try {
            file.setLastModified(setTo);
        } catch (SecurityException e) {
            throw new FailingException();
        }
    }


    /**
     * returns the line in the command file
     * @return 
     */
    @Override
    public int getLine() {
        return line;
    }
}