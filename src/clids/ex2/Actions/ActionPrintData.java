/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Actions;

import clids.ex2.ToolBox;
import java.io.*;

/**
 *
 * @author t7638506
 */
public class ActionPrintData implements Action {

    int line;

    /**
     * a standart Ctor
     *
     * @param line the line of the command file where this action was written
     */
    public ActionPrintData(int line) {
        this.line = line;
    }
/**
 * the action to do
 * @param file the file to print its data
 */
    @Override
    public void doAction(File file) {
        ToolBox.printData(file);

    }
    /**
     * returns the line in the command file
     * @return the line
     */
    @Override
    public int getLine() {
        return line;
    }
}
