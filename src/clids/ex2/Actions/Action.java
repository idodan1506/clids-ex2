package clids.ex2.Actions;
import clids.ex2.Exceptions.*;
import java.io.*;

public interface Action
{
	public void doAction(File file)throws  FailingException;// the action to be done
        
        public int getLine();
}
