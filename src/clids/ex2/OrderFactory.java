/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2;

import clids.ex2.Exceptions.BadOrderNameException;
import clids.ex2.Order.*;

/**
 *
 * @author t7638506
 */
public class OrderFactory {

    /**
     * returns an order based on the values and name in the String.
     *
     * @param toDo
     * @return a pointer of the interface "action" that is of an instance
     * specified in the command file.
     * @throws BadOrderNameException
     */
    public static Order getOrder(String toDo) throws BadOrderNameException {
        Order order = null;
        switch (toDo) {// figuring wich order needed to be created.
            case "abs":
                order = new OrderPath();
                break;
            case "file":
                order = new OrderName();
                break;

            case "mod":
                order = new OrderDate();
                break;
            case "size":
                order = new OrderSize();
                break;
            default:
                throw new BadOrderNameException();
        }
        return order;
    }
}
