/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2;

/**
 *
 * @author t7638506
 */
import java.io.*;
import java.util.*;
import clids.ex2.Actions.*;
import clids.ex2.Exceptions.BadActionNameException;
import clids.ex2.Exceptions.BadInputParamsException;

public class ActionFactory {

    /**
     * returns an actions based on the values and name in the String.
     *
     * @param toDo
     * @return a pointer of the interface "action" that is of an instance
     * specified in the command file.
     * @throws BadActionNameException
     * @throws BadInputParamsException
     */
    public static Action getAction(String sourceDir, String toDo, int line) throws BadActionNameException, BadInputParamsException {
        int reader;
        reader = toDo.indexOf('%');
        String command;
        String values;
        if (reader != -1) {
            command = toDo.substring(0, reader);
            values = toDo.substring(reader + 1);
        } else {
            command = toDo;
            values = null;
        }
        boolean value;
        Action action = null;
        switch (command) {// figuring wich action to do.
            case "print_name":
                action = new ActionPrintName(line);
                break;
            case "print_data":
                action = new ActionPrintData(line);
                break;

            case "copy":
                File targetDir = new File(sourceDir,values);
                action = new ActionCopy(targetDir, line);
                break;
            case "exec":
                value = ToolBox.stringToBoolean(values);
                action = (Action) new ActionSetExec(value, line);
                break;
            case "write":
                value = ToolBox.stringToBoolean(values);
                action = (Action) new ActionSetWritable(value, line);
                break;
            case "last_mod":
                Date date = ToolBox.createDateFromStringByFormat(values);
                action = (Action) new ActionSetDate(date.getTime(), line);
                break;

        }
        if (action == null) {//only null if a bad string.
            throw new BadActionNameException();
        }
        return action;
    }
    
    public static Action getAction(String toDo, int line) throws BadActionNameException, BadInputParamsException {
        return getAction("",toDo,line);
    }
}
