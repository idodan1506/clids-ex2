package clids.ex2;

import clids.ex2.Exceptions.BadInputParamsException;
import clids.ex2.Exceptions.FailingException;
import clids.ex2.Filters.AcceptAllFilter;
import clids.ex2.Filters.Filter;
import clids.ex2.Order.Order;
import clids.ex2.Order.OrderPath;
import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ToolBox {

    public static final Order DEFAULT_ORDER = new OrderPath();
    public static final Filter DEFAULT_FILTER = new AcceptAllFilter();
    public static final String WARNING = "Warning in line ";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final int MILLISECONDS_IN_AN_HOUR = 3600000;
/**
     * copies the fileToCopy parameter to the targetDir. doing so using the nio copy
     * method.
     *
     * @param fileToCopy-the file to copy
     * @param targetDir-the path to copy it to
     * @throws FailingException
     */
    public static void copy(File fileToCopy, File targetDir) throws FailingException {
        try {
            Path in = fileToCopy.toPath();
            Path out = targetDir.toPath();
            Files.copy(in, out);
        } catch (IOException ex) {
            throw new FailingException();
        }
    }
/**
     * prints the name of the file given as a parameter.
     *
     * @param file-to print
     */
    public static void printName(File file) {
        System.out.println(file.getName() + "\n");
    }

    /**
     * prints the data of the file given as a parameter.
     *
     * @param file-to print
     */

    public static void printData(File file) {
        String data = "";
        if (file.isHidden()) {
            data += "h";
        } else {
            data += "-";
        }

        if (file.canWrite()) {
            data += "w";
        } else {
            data += "-";
        }

        if (file.canExecute()) {
            data += "x";
        } else {
            data += "-";
        }
        double fileSize = bytesToKilo(file.length());
        Date lastMod = new Date(file.lastModified());
        data += " " + fileSize + " " + lastMod.toString() +" ";
        data += file.getAbsolutePath();
        System.out.println(data);
                
        

    }

    /**
     * sets the executable state of the file to be like the given boolean
     * parameter
     *
     * @param file-the file to change
     * @param isExec-the state to change it to.
     */

    public static void setExec(File file, boolean isExec) {
        file.setExecutable(isExec);
    }
    /**
     * sets the writable state of the file to be like the given boolean
     * parameter
     *
     * @param file-the file to change
     * @param isewritable-the state to change to
     */

    public static void setWritable(File file, boolean iswritable) {
        file.setExecutable(iswritable);
    }

    /**
     * sets the last modification date of the file to be like the given long
     * parameter
     *
     * @param file
     * @param time-the new date
     */

    public static void lastMod(File file, long time) {
        file.setLastModified(time);
    }
    /**
     * returns the number of kilo byte for a given number of bytes
     *
     * @param bytes-the byte to convert
     * @return-the size in kb
     */

    public static double bytesToKilo(double bytes) {
        return bytes / 1024;
    }

    /**
     * return true or false if the String equals "YES" or "NO". if the String is
     * not one of the above throw an exception.
     *
     * @param value-the value to convert
     * @return the value in boolean
     * @throws BadInputParamsException
     */

    public static boolean stringToBoolean(String value) throws BadInputParamsException {
        switch (value) {
            case "YES":
                return true;
            case "NO":
                return false;
            default:
                throw new BadInputParamsException();
        }

    }
    /**
     * returns all the files in the sub directories (if the are any) in a given
     * path.
     *
     * @param files the file to mine from
     * @return the array of the file in the sub directories.
     */

    public static File[] returnFileArray(ArrayList<File> files) {
        File[] filesArray = new File[(files.size())];
        for (int i = 0; i < filesArray.length; i++) {
            filesArray[i] = files.get(i);

        }
        return filesArray;
    }

    /**
     * converts a String of the format of DATE_FORMAT to a date object
     *
     * @param dateStr should look like DATE_FORMAT
     * @return Date object 
     * 
     */

    public static Date createDateFromStringByFormat(String dateStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date toReturn = null;
        try {
            toReturn = dateFormat.parse(dateStr);
        } catch (ParseException ex) {
            System.err.println("Date parser Error");
            System.exit(-1);
        }
        return toReturn;
    }
}
