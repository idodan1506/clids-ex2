/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Exceptions;

/**
 * An abstract class for all the exceptions for actions that could not operate
 *
 * @author Administrator
 */
public class FailingException extends Exception {

    private static final long serialVersionUID = 1L;
    private int lineNumber;

    /**
     * Ctor of the default instance
     */
    public FailingException() {
        super();
    }

    /**
     * Ctor of an instance that holds its lineNumber
     *
     * @param lineNumber
     */
    public FailingException(int lineNumber) {
        super();
        this.lineNumber = lineNumber;
    }

    /**
     * Operating the exception
     */
    public void handle() {
        System.err.println("Action failed in line " + lineNumber);
    }

    public void handle(int lineNumber) {
        System.err.println("Action failed in line " + lineNumber);
    }
}
