/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Exceptions;

/**
 * An abstract exception class for exceptions that cause program collapsing
 *
 * @author Administrator
 */
public abstract class FatalException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Ctor of the default instance
     */
    protected FatalException() {
        super();
    }

    /**
     * Operating the exception
     */
    public void handle() {
        System.err.println("ERROR");
        System.exit(-1);
    }
}
