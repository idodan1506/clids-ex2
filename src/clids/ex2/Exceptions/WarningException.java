/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Exceptions;

/**
 * An abstract exceptions for the exception that causes printing a warning
 *
 * @author Administrator
 */
public class WarningException extends Exception {

    protected static final long serialVersionUID = 1L;
    private int lineNumber;

    /**
     * Ctor of the default instance
     */
    public WarningException() {
        super();
    }

    /**
     * Ctor of an instance that holds its lineNumber
     *
     * @param lineNumber
     */
    public WarningException(int lineNumber) {
        super();
        this.lineNumber = lineNumber;
    }

    /**
     * Operating the exception
     */
    public void handle() {
        System.err.println("Warning in line " + lineNumber);
    }
}
