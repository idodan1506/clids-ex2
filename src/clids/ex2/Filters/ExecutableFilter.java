/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;

/**
 * A filter class that accepts executable files and all directories
 *
 * @author Administrator
 */
public class ExecutableFilter extends BooleanFilter {

    /**
     * Ctor of the class
     *
     * @param executable - whether or not accept executable files
     */
    public ExecutableFilter(boolean executable) {
        super(executable);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathName) {

        // checks if the input equals to the file execution state
        return ((inputYesNo == pathName.canExecute()) ? true : false);
    }
}
