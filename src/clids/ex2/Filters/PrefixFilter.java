/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;

/**
 * A filter class that checks if the file has the given prefix and accepts all
 * directories
 *
 * @author Administrator
 */
public class PrefixFilter extends StringFilter {

    /**
     * the Ctor of class
     *
     * @param prefix - the prefix to check
     */
    public PrefixFilter(String prefix) {
        super(prefix);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathName) {

        // throws exeptions for directories
        return (pathName.getName().startsWith(inputName));
    }
}
