/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;

/**
 * A filter class that accepts files according to their writing permission and
 * all directories
 *
 * @author Administrator
 */
public class WritableFilter extends BooleanFilter {

    /**
     * the Ctor of the class
     *
     * @param writable - whether or not the class is writable
     */
    public WritableFilter(boolean writable) {
        super(writable);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    // checks if the input equals to the file writing state
    @Override
    public boolean specificAccept(File pathName) {
        return ((inputYesNo == pathName.canWrite()) ? true : false);
    }
}
