/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

/**
 * An abstract class for all the string based filters
 *
 * @author Administrator
 */
public abstract class StringFilter extends Filter {
    // the value of the filter

    protected String inputName;

    protected StringFilter(String inputName) {
        this.inputName = inputName;
    }
}