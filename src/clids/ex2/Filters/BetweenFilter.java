/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import clids.ex2.ToolBox;
import java.io.File;

/**
 * A filter class that accepts files in size between two given values and all
 * directories
 *
 * @author Administrator
 */
public class BetweenFilter extends DoubleFilter {

    /**
     * Ctor of the filter
     *
     * @param small - the lower value
     * @param large - the upper value
     */
    public BetweenFilter(double small, double large) {

        // for small = val1, large = val2
        super(small, large);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathname) {
        // if the file size is smaller than val2 and greater than val 1 return
        // true
        return (ToolBox.bytesToKilo(pathname.length()) >= val1 && ToolBox.
                bytesToKilo(pathname.length()) <= val2);
    }
}
