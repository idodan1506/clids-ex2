/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author Administrator
 */
public abstract class Filter implements FileFilter{
    
    @Override
    public boolean accept(File pathName){
        return (pathName.isDirectory() || specificAccept(pathName));
    }
    
    public abstract boolean specificAccept(File pathName);
}
