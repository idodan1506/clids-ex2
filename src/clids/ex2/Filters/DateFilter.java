/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.util.Date;

/**
 * An abstract class for all the date based filters
 *
 * @author Administrator
 */
public abstract class DateFilter extends Filter {

    // the date value of the filter
    protected Date inputDate;

    protected DateFilter(Date inputDate) {
        this.inputDate = inputDate;
    }
    
}
