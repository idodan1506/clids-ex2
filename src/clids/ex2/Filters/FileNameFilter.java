/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;

/**
 * A filter class that accepts files that have a given name and all directories
 *
 * @author Administrator
 */
public class FileNameFilter extends StringFilter {

    /**
     * The Ctor of the class
     *
     * @param fileName - the name of the file
     */
    public FileNameFilter(String fileName) {
        super(fileName);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathName) {

        // throws directories
        return (pathName.getName().equals(inputName));
    }
}
