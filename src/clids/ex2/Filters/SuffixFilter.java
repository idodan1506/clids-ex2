/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;

/**
 * A filter class that checks if the file has the given prefix and accepts all
 * directories
 *
 * @author Administrator
 */
public class SuffixFilter extends StringFilter {

    /**
     * the ctor of the class
     *
     * @param suffix - the suffix to check
     */
    public SuffixFilter(String suffix) {
        super(suffix);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathName) {

        // throws directories
        return (pathName.getName().endsWith(inputName));
    }
}
