/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;

/**
 * A filter class that filters hidden classes and accepts all directories
 *
 * @author Administrator
 */
public class HiddenFilter extends BooleanFilter {

    /**
     * The Ctor of the class
     *
     * @param hidden - whether or not accept hidden files
     */
    public HiddenFilter(boolean hidden) {
        super(hidden);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathName) {

        // checks if the input equals to the file hidding state
        return ((inputYesNo == pathName.isHidden()) ? true : false);
    }
}
