/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;
import java.util.Date;

/**
 * A filter class that accepts all files last modified before an input date and
 * all directories
 *
 * @author Administrator
 */
public class BeforeFilter extends DateFilter {

    public BeforeFilter(Date date) {
        super(date);
    }

    @Override
    public boolean specificAccept(File pathName) {

        Date fileDate = new Date(pathName.lastModified());
        if (fileDate.before(inputDate) || fileDate.compareTo(inputDate) == 0) {
            return true;
        }
        return false;
    }
}