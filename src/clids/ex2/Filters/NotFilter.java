/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import java.io.File;

/**
 * A filter class that accepts the opposite files of another filter
 *
 * @author Administrator
 */
public class NotFilter extends Filter {

    private Filter filter;

    /**
     * The Ctor of the class
     *
     * @param filter - the filter to return its opposite
     */
    public NotFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathName) {
        return (!filter.accept(pathName));
    }
}
