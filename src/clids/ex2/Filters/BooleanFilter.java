/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

/**
 * An abstract class for all the boolean based filter
 *
 * @author Administrator
 */
public abstract class BooleanFilter extends Filter {
    // the value of the filter

    protected boolean inputYesNo;

    protected BooleanFilter(boolean inputYesNo) {
        this.inputYesNo = inputYesNo;
    }
}
