/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;


/**
 * An abstract class for all the double based filters
 *
 * @author Administrator
 */
public abstract class DoubleFilter extends Filter {

    // the value of the filter
    protected double inputNumber, val1, val2;

    protected DoubleFilter(double inputNumber) {
        this.inputNumber = inputNumber;
    }

    protected DoubleFilter(double val1, double val2) {
        this.val1 = val1;
        this.val2 = val2;
    }
}
