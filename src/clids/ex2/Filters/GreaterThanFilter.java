/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2.Filters;

import clids.ex2.ToolBox;
import java.io.File;

/**
 * A filter class that accepts files in size equal or greater than given value
 * and all directories
 *
 * @author Administrator
 */
public class GreaterThanFilter extends DoubleFilter {

    /**
     * Ctor of the class
     *
     * @param inputNumber - the size in k-bytes to check if greater than
     */
    public GreaterThanFilter(double inputNumber) {
        super(inputNumber);
    }

    /**
     * Checks if the file is accepted by the filter
     *
     * @param pathname - the path of the file
     * @return true if the file is acceptable false if not
     */
    @Override
    public boolean specificAccept(File pathname) {

        //throws exeptions of folder and not file or path does not exist
        if (ToolBox.bytesToKilo(pathname.length()) > inputNumber) {
            return true;
        }
        return false;
    }
}
