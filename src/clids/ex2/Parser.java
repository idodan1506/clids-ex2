/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex2;

import clids.ex2.Exceptions.BadSubsectionException;
import clids.ex2.Exceptions.FirstLineException;
import clids.ex2.Exceptions.WarningException;
import clids.ex2.Filters.AcceptAllFilter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;


/**
 *
 * @author Administrator
 */
public class Parser {

    private Reader myReader;
    private BufferedReader myBufferReader;
    private ArrayList<ArrayList<Integer>> lineNumbers;
    private ArrayList<String> lineBlock;
    private ArrayList<ArrayList<String>> rawSectionsHolder;
    // a counter to know in which section we are
    private int sectionCounter = -1;
    private File sourceDir;

    // the constractor of parser
    public Parser(File commandFile, File sourceDir) {

        this.sourceDir = sourceDir;
        // tries to read the file and if is corrupted prints error
        try {
            myReader = new FileReader(commandFile);
        } catch (FileNotFoundException e) {
            System.err.println("ERROR");
            System.exit(-1);
        }

        myBufferReader = new BufferedReader(myReader);
    }

    /**
     * A function that returns a String array containing all the commands from
     * the commandFile
     *
     * @return a string array that contains the lines from the command file
     */
    private void lineBlock() {

        // creating an array list
        lineBlock = new ArrayList<>();

        try {
            // while the file is not empty, add the next line to the arrayList
            while (myBufferReader.ready()) {
                lineBlock.add(myBufferReader.readLine());
            }

        } // cathing error type 2 and printing error.
        catch (IOException e) {
            System.err.println("ERROR");
            System.exit(-1);
        } finally {
            try {
                myBufferReader.close();
            } catch (IOException ex) {
                System.err.println("ERROR");
                System.exit(-1);
            }
        }
    }

    // a function that divides the lineBlock which is the file seperated to lines,
    // into sections seperated by the line "FILTER"
    private void divideToSections() throws FirstLineException {

        //throw the case in which the file starts with nothing else than "FILTER"
        if (!lineBlock.get(0).equals("FILTER")) {
            throw new FirstLineException();
        }

        // creating an arrayList for holding the divided sections
        rawSectionsHolder = new ArrayList<>();
        lineNumbers = new ArrayList<>();
        int sectionsCounter = -1;
        int lineNumber = 1;
        // every time the line is "FILTER" it inserts the line and all the
        //following ones untill the next "FILTER" to the same sub ArrayList
        for (String line : lineBlock) {
            if (line.equals("FILTER")) {
                rawSectionsHolder.add(new ArrayList<String>());
                lineNumbers.add(new ArrayList<Integer>());
                sectionsCounter++;
            }
            lineNumbers.get(sectionsCounter).add(lineNumber);
            rawSectionsHolder.get(sectionsCounter).add(line);
            lineNumber++;
        }
    }
    // a function that check for every sub ArrayList if valid and feeds it to
    // a Section type object

    private Section checkSection(ArrayList<String> rawSection) throws
            BadSubsectionException {

        // creating new deault section
        Section sec = new Section();

        sectionCounter++;
        // initializing counters 

        // counter for the line "ACTION"
        int bigActionTimes = 0;

        // counter for number of filters
        int filtersNumber = 0;

        //counter for number of orders
        int orderNumber = 0;

        //counter for the line "ORDER"
        int bigOrderTimes = 0;

        // counter for the line "FILTER"
        int bigFilterTimes = 0;

        // a counter to know in which line in the section we are
        int lineIndexCounter = 0;

        // running over the lines in a raw section to check it and feed to a 
        //section type object
        for (String line : rawSection) {

            // checking comment
            if (line.startsWith("@")) {
                sec.addComment(line);
            } else if (line.equals("ORDER")) {
                bigOrderTimes++;
            } else if (line.equals("ACTION")) {
                bigActionTimes++;
            } else if (line.equals("FILTER")) {
                bigFilterTimes++;
            } else {

                // tries to check and do the actions and considers exceptions
                try {
                    if (bigActionTimes == 0 && bigFilterTimes == 1 && bigOrderTimes==0) {
                        // is a filter
                        filtersNumber++;
                        sec.setFilter(FilterFactory.getFilter(line));

                    } else if (bigActionTimes == 1 && bigFilterTimes == 1
                            && bigOrderTimes == 0) {
                        // is an action
                        sec.addAction(ActionFactory.getAction(sourceDir.getPath(),line, lineNumbers
                                .get(sectionCounter).get(lineIndexCounter)));

                    } else if (bigActionTimes == 1 && bigFilterTimes == 1
                            && bigOrderTimes == 1) {
                        // is an order
                        orderNumber++;

                        sec.setOrder(OrderFactory.getOrder(line));
                    }

                } catch (WarningException e) {
                    sec.addError(ToolBox.WARNING + lineNumbers.get(sectionCounter)
                            .get(lineIndexCounter));
                }
            }
            lineIndexCounter++;

        }

        // inserting defaultOrder
        if (bigOrderTimes == 0 || orderNumber == 0) {
            sec.setOrder(ToolBox.DEFAULT_ORDER);
        }

        // final check
        if (bigActionTimes != 1 || bigOrderTimes > 1 || filtersNumber > 1
                || orderNumber > 1) {

            throw new BadSubsectionException();
        }

        if (sec.getFilter() == null) {
            sec.setFilter(new AcceptAllFilter());
        }

        return sec;

    }

    public ArrayList<Section> parse() throws BadSubsectionException, FirstLineException {
        ArrayList<Section> toReturn = new ArrayList<>();
        lineBlock();
        divideToSections();
        for (ArrayList<String> rawSection : rawSectionsHolder) {
            toReturn.add(checkSection(rawSection));
        }
        return toReturn;

    }
}
