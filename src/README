idan12
yrmi

====================
= FILE DESCRIPTION =
====================
clids.ex2
---------
    ActionFactory.java - the factory that converts action String to actions
    FilterFactory.java - the factory that converts filter String to filter
    Manager.java - the class that holds the "main" operates the program
    Parser.java - the class that parses the commandFile into a Section data
                    type that contains filter, actions, order and comments
    Section.java - the class of the Section data type as described above
    ToolBox.java - a class that contains static common functions
    README.java - this file

clids.ex2.Filters
-----------------
    AfterFilter.java - class of filter that filters files last modified after a 
                       given date
    BeforeFilter.java - class of filter that filters files last modified before
                        a given date
    BetweenFilter.java - class of filter that filters files with size in k bytes
                         between 2 given values
    BooleanFilter.java - an abstract parent class for all the Boolean based 
                         filters
    DateFilter.java - an abstract parent class for all the Date based 
                      filters
    DoubleFilter.java - an abstract parent class for all the Double based 
                        filters
    ExecutableFilter.java - class of filter that filters files by their Execution
                            permission
    FileNameFilter.java - class of filter that filters files by a certain name

    Filter.java - an abstract parent class for all filters

    GreaterThanFilter.java - class of filter that filters files in size bigger
                             than a given value
    HiddenFilter.java - class of filter that filters files by their hiding 
                        state
    NotFilter.java - A filter class that accepts the opposite files of another 
                     filter
    PrefixFilter.java - class of filter that filters files that has a certain
                        prefix
    SmallerThanFilter.java - class of filter that filters files in size smaller
                             than a given value                  
    StringFilter.java - an abstract parent class for all the String based 
                        filters
    Suffixfilter.java - class of filter that filters files that has a certain
                        suffix
    WritableFilter.java - class of filter that filters files by their writing 
                          permission

clids.ex2.Actions
-----------------
    Action.java - Action interface that is implemented by all Action classes
    ActionCopy. java - an action that conducts the copy operation
    ActionPrintData.java - an action that conducts data printing operation
    ActionPrintName.java - an action that conducts name printing operation
    ActionSet.java - parent class for date, exec and writable actions
    ActionSetDate.java - an action that sets the modification date of a file
    ActionSetExec.java - an action that sets the Execution permissions of a file
    ActionSetWritable.java - an action that sets the writing permissions of a file
    
clids.ex2.Exceptions
--------------------
    BadActionNameException.java - exception for invalid action name
    BadFilterNameException.java - exception for invalid filter name
    BadOrderNameException.java - exception for invalid order name
    BadInputParamsException.java - exception for invalid input params
    BadSubSectionException.java - exception for bad subsection name or order
    BetweenIlligalValueException.java - exception for illigal values in between
                                        filter
    FailingException.java - exception for failing actions
    IlligalCopyException.java - excption for illegal copy action
    InvalidUsageException.java - exception for invalid usage of the program
    WarningException.java - an abstract class for all the warning exceptions

clids.ex2.Order
---------------
    Order.java - an interface implemented by all orders
    OrderDate.java - a date based ordering
    OrderName.java - a name based ordering
    orderPath.java - a path based ordering
    OrderSize.java - a size based ordering

==========
= DESIGN =
==========

We will explain the design in parts
-----------------------------------

MacroDesign
-----------
The input command file and source directory are transfered to the manager.
The manager calls to parser with the command file and the parser sorts the lines
of the command file into sections, validate them, and using the factories
receives the actions filter and order instances that are saved in a "Section" 
data type along with the line numbers of each instance and the comments.
The Sections are returned to the manager which filter the classes and operates
the actions using the given order.
The exceptions are caught in the manager which prints them in the right time
if necessary.
Furthermore, there is a toolbox that contain common functions and used by all
classes.

Our design is almost identical to the one showd in class. One difference is that
our "Section" holds the actions, filter and order instances received from the 
factories and the manager operates the functions

MicroDesign - filters
-----------
All the filters implements the FileFilter interface of java and extends the
Filter abstract class. The classes DoubleFilter, StringFilter, DateFilter and
BooleanFilter are superClasses of all the other filters according to the input
type. That way the filters are more modulated and making similar filters in the
future is easier.

MicroDesign - actions
-----------
All the actions implements a self built interface called "Action" and the 
action classes that sets states extends the class ActionSet.

